import React, { useState, useEffect } from 'react';
import '../../assets/css/main.css'
import './style.css';
import ImgPost from '../../assets/img/robo.jpg'
import api from '../../services/api'
import { Container } from 'reactstrap';

 const Post = () => {

    const [posts, setPosts] = useState('');

    useEffect(() => {
        api.get(`/posts`, {
        }).then(response => {
        setPosts(response.data)
        })
    })
    console.log(posts);
    
    return (
        <Container>
            <section id="info-post">
                <h2>{posts.title}</h2>
                <h3>Author: </h3>
            </section>
            <section className="conteudo-post">
                <p></p>
            </section>
        </Container>
    )
}

export default Post