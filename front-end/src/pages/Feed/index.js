import React from 'react';
import '../../assets/css/main.css'
import './style.css'
import CardFeed from '../../components/CardFeed'
import Search from '../../components/Search'

const Feed = () => {
    return(
        <>
         <h2 className="title">Feed</h2>
         <Search />
         <CardFeed />
        </>
    )
}

export default Feed