import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Login from './pages/Login'
import Cadastro from './pages/Cadastro'
import Perfil from './pages/Perfil';
import Post from './pages/Post'
import Feed from './pages/Feed'

const Routes = () => (
    <Switch>
        <Route path='/cadastro' component={Cadastro} />
        <Route path='/login' component={Login} />
        <Route path='/user/:id' component={Perfil} />
        <Route path='/posts/:id' component={Post} />
        <Route path='/feed' component={Feed} />
    </Switch>
)

export default Routes;