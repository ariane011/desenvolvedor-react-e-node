import React from 'react';
import { Link } from "react-router-dom";
import { Navbar, NavbarBrand } from 'reactstrap';
import '../../assets/css/main.css'
import './style.css'
import logo from "../../assets/img/logo-eleven.svg";

const NavBar = () => {

  return (
    <div>
      <Navbar className="navbar" expand="md">
        <NavbarBrand className="nav-brand" href="/" data-placement="bottom" title="Logo Eleven">
          <img src={logo} className="logo" alt="Logo Eleven"></img>
          <Link class="link-login" to={`/login`}>Login</Link>
        </NavbarBrand>
      </Navbar>
    </div>
  );
}

export default NavBar;