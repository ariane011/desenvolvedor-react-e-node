import React, { useState, useEffect } from 'react';
import { Card, CardImg, CardText, CardDeck, CardBody, CardTitle
} from 'reactstrap';
import '../../assets/css/main.css'
import './style.css';
import ImgPost from '../../assets/img/robo.jpg'
import api from '../../services/api'

 const CardFeed = (props) => {

    const [post, setPost] = useState([]);
    const id = localStorage.getItem('id');

    useEffect(() => {
        api.get('/posts', {
            headers: {
                Authorization: id,
            }
        }).then(response => {
            const posts = response.data;
            let listaPosts = [];
            for (let i = 0; i < posts.length; i++){
                if (posts[i].status === 1 && props.status === true){
                    listaPosts.push(posts[i]);
                }
                else if (posts[i].status === 0 && props.status === false) {
                    listaPosts.push(posts[i]);
                }
            }
            setPost(posts)
        })
    }, [id])


    return (
        <>
        {post.map(posts => (
            <CardDeck key={posts.id} className="card-deck">
                <Card className="card">
                    <CardBody className="card-body">
                        <CardImg className="card-img" top width="100%" src={ImgPost} alt="Avatar" />
                        <CardTitle className="card-title">{posts.title}</CardTitle>
                        <CardText className="card-text">
                            <p>Data: {posts.date}</p>
                        </CardText>
                    </CardBody>
                </Card>
            </CardDeck>
            ))}
        </>
    );
}

export default CardFeed;