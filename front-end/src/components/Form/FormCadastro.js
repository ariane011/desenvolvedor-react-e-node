import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Label, Input, Container, Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import '../../assets/css/main.css'
import './style.css'
import { useFormik } from "formik";
import * as Yup from "yup";
import api from '../../services/api'

const FormCadastro = () => {

    // Validação do formulário com Yup
const validationSchema = Yup.object().shape({
    name: Yup.string()
    .min(2, 'Nome muito curto')
    .max(150, 'Nome muito grande')
    .required('Obrigatório'),
    email: Yup.string()
      .email('Email inválido')
      .required('Obrigatório'),
    password: Yup.string()
      .min(6, 'Senha muito curta')
      .max(50, 'Senha muito grande')
      .required('Obrigatório'),
    senha_confirmacao: Yup.string()
      .oneOf([Yup.ref('password'), null], 'As senhas devem ser iguais')
      .required('Obrigatório')
  });

    const initialValues = {
        id: "",
        name: "",
        email: "",
        product_id: "",
        password: ""
    };

    const [products, setProducts] = useState([]);

    const history = useHistory();
    const formik = useFormik({
        initialValues,
        validationSchema
      });


    const DisplayErrors = (props) => {
    const { msgError } = props
      return(
          <>
          {msgError && <span className="validate-error">{msgError}</span>}
          </>    
      )
    }

    

    async function handleFormInput(e) {
        e.preventDefault();
        let errors = formik.errors;
        let values = formik.values;
  
        if (Object.keys(errors).length > 0 || values.email === "" ) {
          alert("Os dados devem ser preenchidos corretamente!");
          return;
        }

        const id = values.id
        const name = values.name
        const email = values.email
        const password = values.password
        const product_id = values.product_id

        const data = {
            id,
            name,
            email,
            product_id,
            password
        };

        await api.post('user', data);

        try {
          alert(`Cadastro efetuado com sucesso!`);
            // após o cadastro vai para a página de login
            history.push('/login');
        } catch (error) {
            alert(`Erro ao se cadastrar, tente novamente`)
        }
        console.log(data)
    }

    useEffect(() => {
        api.get(`/products`, {
        }).then(response => {
        setProducts(response.data)
        })
    })
    console.log(products);

    return(
        <Container id="form-input">
            <h2>Registro</h2>
            <div id="box-input" >
              <Form onSubmit={handleFormInput}>
                  <FormGroup>
                    <Label for="name">Nome:</Label>
                    <Input type="text" name="name" id="name" placeholder="Nome Completo" 
                    onChange={formik.handleChange}
                    {...formik.getFieldProps("name")}
                    /> {formik.errors && <DisplayErrors msgError={formik.errors.name}/>}
                  </FormGroup>
                  <FormGroup>
                    <Label for="email">Email:</Label>
                    <Input type="email" name="email" id="email" placeholder="email@mail.com" 
                    onChange={formik.handleChange}
                    {...formik.getFieldProps("email")}
                    /> {formik.errors && <DisplayErrors msgError={formik.errors.email} />}
                  </FormGroup>
                  <FormGroup>
                        <Label for="exampleSelect">Produto</Label>
                        <Input type="select" name="product_id" id="exampleSelect" onChange={formik.handleChange}
                        {...formik.getFieldProps("product_id")}
                        > {formik.errors && <DisplayErrors msgError={formik.errors.product_id}/>}
                            <option value="" selected disabled>Selecione o seu produto
                            </option>
                            {products.map(product => 
                            <option key={product.id}>{product.name}</option>
                            )}
                        </Input>
                  </FormGroup>
                  <FormGroup>
                    <Label for="password">Senha:</Label>
                    <Input type="password" name="password" id="password" placeholder="Insira uma senha de até 6 caracteres" 
                     onChange={formik.handleChange}
                    {...formik.getFieldProps("password")}
                    /> {formik.errors && <DisplayErrors msgError={formik.errors.password}/>}
                  </FormGroup>
                  <FormGroup>
                    <Label for="senha_confirmacao">Repita a Senha:</Label>
                    <Input type="password" name="senha_confirmacao" id="senha_confirmacao" placeholder="Repita a senha..." {...formik.getFieldProps("senha_confirmacao")} />
                    {formik.errors && <DisplayErrors msgError={formik.errors.senha_confirmacao}/>}
                  </FormGroup>
                  <Button type="submit" className="btn-form-registrar" >Registrar</Button>
              </Form>
            </div>
        </Container>   
    )
}

export default FormCadastro;