import React, { useState, useEffect } from 'react';
import { Form, FormGroup, Label, Input, Container, Button } from 'reactstrap';
import { useHistory, useParams } from 'react-router-dom';
import api from '../../services/api'
import { useFormik } from "formik";
import '../../assets/css/main.css'
import './style.css'
import Avatar from '../../assets/img/avatar01.svg'

const FormEditar = () => {
    const initialValues = {
        id: "",
        name: "",
        email: "",
        product_id: "",
        password: ""
    };

    const [products, setProducts] = useState([]);

    const history = useHistory();
    const formik = useFormik({
        initialValues,
    });

    const DisplayErrors = (props) => {
    const { msgError } = props
      return(
          <>
          {msgError && <span className="validate-error">{msgError}</span>}
          </>    
      )
    }

    async function handleFormInput(e) {
        e.preventDefault();
        let errors = formik.errors;
        let values = formik.values;
  
        if (Object.keys(errors).length > 0 || values.email === "" ) {
          alert("Os dados devem ser preenchidos corretamente!");
          return;
        }

        const id = values.id
        const name = values.name
        const email = values.email
        const password = values.password
        const product_id = values.product_id

        const data = {
            id,
            name,
            email,
            product_id,
            password
        };

        await api.put('user', id);

        try {
          alert(`Cadastro efetuado com sucesso!`);
            // após o cadastro vai para a página de login
            history.push('/');
        } catch (error) {
            alert(`Erro ao se cadastrar, tente novamente`)
        }
        console.log(data)
    }

    useEffect(() => {
        api.get(`/products`,  {
        }).then(response => {
        setProducts(response.data)
        })
    })
    console.log(products);

    return(
            <Container id="form-input">
             <img className="avatar" src={Avatar} alt="Avatar"/>
             <h3 className="nome">Nome</h3>
             <div id="box-input" >
            <Form onSubmit={handleFormInput}>
                  <FormGroup>
                    <Label for="email">Email:</Label>
                    <Input type="email" name="email" id="email" placeholder="email@mail.com" 
                    onChange={formik.handleChange}
                    {...formik.getFieldProps("email")}
                    /> {formik.errors && <DisplayErrors msgError={formik.errors.email} />}
                  </FormGroup>
                  <FormGroup>
                        <Label for="exampleSelect">Produto</Label>
                        <Input type="select" name="product_id" id="exampleSelect" onChange={formik.handleChange}
                        {...formik.getFieldProps("product_id")}
                        > {formik.errors && <DisplayErrors msgError={formik.errors.product_id}/>}
                            <option value="" selected disabled>Selecione o seu produto
                            </option>
                            {products.map(product => 
                            <option key={product.id}>{product.name}</option>
                            )}
                        </Input>
                  </FormGroup>
                  <Button type="submit" className="btn-salvar">Salvar</Button>
                  <Button to="/" type="submit" className="btn-sair" >Sair</Button>
              </Form>
            </div>
        </Container>
    )
}

export default FormEditar;