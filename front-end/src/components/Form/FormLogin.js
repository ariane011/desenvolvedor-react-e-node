import React, { useState } from 'react';
import { Form, FormGroup, Label, Input, Container, Button } from 'reactstrap';
import { useHistory, Link } from 'react-router-dom';
import '../../assets/css/main.css'
import './style.css'
import api from '../../services/api'

const FormLogin = () => { 

  const [id, setId] = useState('');
  const history = useHistory();

  async function handleLogin(e){
      e.preventDefault();

      try {
          const response = await api.show('/session');

          localStorage.setItem('email', response.data.email);
          localStorage.setItem('password', response.data.password);

          history.push(`/perfil/${id}`);
          console.log(response.data)
      } catch (error) {
         alert('Falha no login, tente novamente!'); 
      }
  }
  

    return(
        <Container id="form-input">
            <h2>Acessar</h2>
            <div id="box-input" >
              <Form onSubmit={handleLogin}>
                  <FormGroup>
                    <Label for="email">Email:</Label>
                    <Input type="email" name="email" id="email" placeholder="email@mail.com"/>
                  </FormGroup>
                  <FormGroup>
                    <Label for="password">Senha:</Label>
                    <Input type="password" name="password" id="password" placeholder="Insira sua senha"/> 
                    <Link className="link-forget-senha" to={"/cadastrar-senha"}>Esqueci minha senha</Link>
                  </FormGroup>
                  {/* <Link to="/cadastro"> */}
                  <Button type="submit" className="btn-form-acessar">Acessar</Button>
                  {/* </Link> */}
                  <p className="p-registro">Não tem cadastro? Registre-se: </p>
                  <Link to="/cadastro">
                  <Button type="submit" className="btn-form-registrar" 
                  >Registrar</Button>
                  </Link>
              </Form>
            </div>
        </Container> 
    )
}

export default FormLogin;