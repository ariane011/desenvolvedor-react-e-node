import React from 'react';
import './style.css';
import { Container, Form, Row, Col, Input } from "reactstrap";

const Search = () => {

    return(
        <>
        <div className="search">
            <Container>
                <Form>
                   <Input placeholder="Procurar..."></Input>
                </Form>
            </Container>
        </div>
        </>
    )
}

export default Search;