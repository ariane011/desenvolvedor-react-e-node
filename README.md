# :rocket: :computer: Teste Dev - React + Node 



### Cadastro
```
http://localhost:3000/cadastro
```

![demo](./front-end/src/assets/img/cadastro.gif)


### Acesso
```
http://localhost:3000/login
```
Obs: usuário ainda não consegue se logar

![demo](./front-end/src/assets/img/acesso.png)


### Perfil
```
http://localhost:3000/user/1
```
Obs: Dados do usuário não são carregados e nem salvos, por enquanto.

![demo](./front-end/src/assets/img/perfil.png)


### Feed
```
http://localhost:3000/feed
```
Obs: Lista os posts, porém, sem a função de mostrar as que o usuário tem permissão de visualizar, por esse motivo, criei a rota temporária: "/feed"

![demo](./front-end/src/assets/img/feed.gif)


****

### :computer: Tecnologias Utilizadas:
 
- [x] React 
- [x] React router
- [x] Reactstrap
- [x] Formik
- [x] Axios
- [x] Express
- [x] Cors
- [x] REST
- [x] NodeJS
- [x] Git
- [x] Insomnia


****

## :house: Rodar o projeto localmente

## ⚠️ Pré-requisitos: 

- Node.js e NPM instalado

****

Passo 1: Clone o projeto na sua máquina

```sh
git clone https://gitlab.com/ariane011/desenvolvedor-react-e-node.git
```
Passo 2: Acesse a pasta do projeto

```sh
cd desenvolvedor-react-e-node
```

Passo 3: Instale todas as dependências do projeto

```sh
npm install
```

Passo 4: Rode o projeto na sua máquina

```sh
npm start
```

Passo 5: Abra o navegador e visualize o projeto

```sh
http://localhost:3000
```
