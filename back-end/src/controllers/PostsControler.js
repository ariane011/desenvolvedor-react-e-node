const axios = require('axios');

module.exports = {
    async index(request, response) {
        let posts;
        await axios.get('http://localhost:3001/posts')
            .then(function(response) {
                posts = response.data
        });
    return response.json(posts);
    },

    async show(request, response) {
        let user;
        const {id} = request.params;
        const teste = this.index
        // const posts = axios.get(`http://localhost:3001/posts`)
        await axios.get(`http://localhost:3002/user/${id}/${teste}`)
            .then(function(response) {
                user = response.data
        });
    return response.json(user);
    }
};
