const axios = require('axios');
const UserController = require('./UserController');

module.exports = {
    async create(request, response) {
        const { email, password } = request.body;

        if(!email && !password) {
            return response.status(400).json({ error: 'Usuário não encontrado' });
        }

        let user;
        await axios.get(`http://localhost:3002/user`)
        .then(
            response => { 
                user = response.data.find(user => 
                    user.email == email
            )}
        );

        if(user.email && email != ''){
            response.json(user)
        }
        else {
            alert("Usuário não encontrado!")
        }
    }
}