const axios = require('axios');

module.exports = {
    // consumindo a API e recebendo dados do json na products
    async index(request, response) {
        let products;
        await axios.get('http://localhost:3002/products')
            .then(function(response) {
                products = response.data
        });
    return response.json(products);
    },

    async show(request, response) {
        let product;
        const {id} = request.params;
        await axios.get(`http://localhost:3002/products/${id}` )
            .then(function(response) {
                product = response.data
        });
    return response.json(product);
    }
};
