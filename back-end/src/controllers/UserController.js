const axios = require('axios');
// const PostsControler = require('./controllers/PostsControler')

module.exports = {
    // consumindo a API e recebendo dados do json no user
    async show(request, response) {
        let user;
        const {id} = request.params;
        await axios.get(`http://localhost:3002/user/${id}`)
            .then(function(response) {
                user = response.data
        });
    return response.json(user);
    },

    async create(request, response){

        const { id, name, email, avatar, password, product_id } = request.body;
        const result = { id, name, email, avatar, password, product_id } 

        await axios.post(`http://localhost:3002/user`, result)
        return response.status(201).send();
    },

    async update(request, response){
        const {id} = request.params;
        const { name, email, avatar, password, product_id } = request.body;
        const result = {id, name, email, avatar, password, product_id } 

        await axios.put(`http://localhost:3002/user/${id}`, result)
        return response.status(201).send();
    },
};