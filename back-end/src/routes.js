// import ProductController from './controllers/ProductController'
const express = require('express');
const PostsControler = require('./controllers/PostsControler')
const ProductController = require('./controllers/ProductController')
const UserController = require('./controllers/UserController');
const SessionController = require('./controllers/SessionController');

const routes = express.Router()

routes.post('/api/v1/session', SessionController.create);
routes.get('/api/v1/posts/', PostsControler.index);
routes.get('/api/v1/user/:id/posts', PostsControler.show);
routes.get('/api/v1/products', ProductController.index);
routes.get('/api/v1/products/:id', ProductController.show);
routes.get('/api/v1/user/:id', UserController.show);
routes.post('/api/v1/user', UserController.create);
routes.put('/api/v1/user/:id', UserController.update);

module.exports = routes;
