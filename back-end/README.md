# Teste Dev - React + Node
Olá pessoa, o seu teste é composto de algumas partes:

    ✔ Aplicação backend consumindo APIs;
    ✔ Aplicação frontend consumindo API desenvolvida por você;
    
## Hold your horses 🐎🐎🐎
Antes de mais nada, você precisa instalar uma dependência global para executar as APIs que irá consumir:
```sh
$ npm install -g json-server
```

## APIs de consumo
A lib `json-server` irá criar um backend servindo alguns endpoints baseados em arquivos com objetos JSON. 
Para isto, execute os comandos em **terminais distintos**:

```sh
# Serviço de blog
$ json-server --watch data/blog.json --port 3001

# Serviço de datalake
$ json-server --watch data/datalake.json --port 3002
```

Os comandos acima disponibilizarão alguns endpoints para você:

**Blog**

- **GET** `http://localhost:3001/posts/`
- **GET** `http://localhost:3001/posts/<id do post>`



**Datalake**

- **GET** `http://localhost:3002/products`
- **GET** `http://localhost:3002/products/<id do produto>`
- **GET** `http://localhost:3002/user`
- **GET** `http://localhost:3002/user/<id do usuário>`
- **DELETE** `http://localhost:3002/user/<id do usuário>`
- **PUT** `http://localhost:3002/user/<id do usuário>`

Consute a [documentação do server-json](https://github.com/typicode/json-server) para enterder como funcionam os [parâmetros de consulta](https://github.com/typicode/json-server#filter).

Com os endpoints funcionais, vamos as suas tarefas.. 🤞🤞


## Aplicação Backend
Sua aplicação backend deverá consumir as informações de ambas APIs e expor alguns endpoints:

- **GET** `api/v1/products`
- **GET** `api/v1/products/<id do produto>`
- **POST** `api/v1/user/`
- **GET** `api/v1/user/<id do usuário>`
- **PUT** `api/v1/user/<id do usuário>`
- **GET** `api/v1/user/<id do usuário>/posts`
- **GET** `api/v1/user/<id do usuário>/posts/<id do post>`

Em sua maioria eles irão expor as informações dos endpoints dos serviços Blog e Datalake.

Já o endpoint **GET** `api/v1/user/<id do usuário>/posts` deve pegar o usuário e buscar somente os posts que possuem o ID do produto (no atributo `product_id` do usuário) em seu atributo `products`. Os posts que o produto do usuário não possui acesso não devem ser retornados.

## Aplicação Frontend
Sua aplicação frontend será composta por cinco telas:
- Cadastro
- Login
- Perfil
- Feed (com busca dinâmica)
- Post

### Cadastro
Tela deverá cadastrar usuários através do endpoint **POST** `api/v1/user/`

<h1 align="center">
  <img alt="backendadmin" src="assets/Registrar.jpg" />
</h1>

### Login
Tela deverá consultar usuários por usuário e senha no endpoint **GET** `api/v1/user/`

<h1 align="center">
  <img alt="backendadmin" src="assets/Login.jpg" />
</h1>

### Perfil
Deverá consultar informações do usuário no endpoint **GET** `api/v1/user/<id do usuário>`

<h1 align="center">
  <img alt="backendadmin" src="assets/Perfil.jpg" />
</h1>

💡 A imagem do usuário pode ser estática e escolhida por você.


### Feed (com busca dinâmica)
Utilizará o endpoint **GET** `api/v1/user/<id do usuário>/posts` para consultar as publicações que o usuário tem acesso.

<h1 align="center">
  <img alt="backendadmin" src="assets/Feed.png" />
</h1>

💡 A imagem dos posts podem ser estáticas e escolhidas por você.

### Post
Utilize o endpoint **GET** `api/v1/user/<id do usuário>/posts/<id do post>` para buscar um post específico.

<h1 align="center">
  <img alt="backendadmin" src="assets/Post.jpg" />
</h1>

💡 A imagem do post pode ser estática e escolhida por você.

## O que vamos avaliar?

- Projeto funcional
- Qualidade do código
- Organização
- Zelo no desenvolvimento

## Finalizando o projeto 😎
Para submeter o seu teste, crie um fork do projeto em seu repositório e envie o link para o email [rh@elevenfinancial.com](mailto:rh@elevenfinancial.com).

Boa sorte 🤙🤙